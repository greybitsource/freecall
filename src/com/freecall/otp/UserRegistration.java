package com.freecall.otp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.freecall.server.ServerInterface;
import com.matesnetwork.Cognalys.VerifyMobile;

import org.linphone.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

/**
 * Created by avinash.malipatil on 24-Jun-16.
 */
public class UserRegistration extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.freecall_user_registration);
        setCountriesList();
        PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void setCountriesList() {

        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for( Locale loc : locale ){
            country = loc.getDisplayCountry();
            if( country.length() > 0 && !countries.contains(country) ){
                countries.add( country );
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

        Spinner countriesSpinner = (Spinner)findViewById(R.id.userreg_spinner_country);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, countries);
        countriesSpinner.setAdapter(adapter);
    }

    public void doVerify(View view) {

        //TODO: Check if network connectivity exists. OTP SDK expects us to call its APIs only when
        // valid SIM is inserted and connected to network

        if(!((CheckBox)findViewById(R.id.userreg_checkBox_agree)).isChecked()) {
            Toast.makeText(this, getString(R.string.freecall_userreg_err_accept_terms), Toast.LENGTH_SHORT).show();
            return ;
        }

        String mobileno = ((EditText)findViewById(R.id.userreg_edit_mobilenum)).getText().toString();
        if(mobileno == null || mobileno.isEmpty()) {
            Toast.makeText(this, getString(R.string.freecall_userreg_err_mobile_number), Toast.LENGTH_SHORT).show();
            return ;
        }

        Intent in = new Intent(this, VerifyMobile.class);
        in.putExtra("app_id", "bdd4f84ba05f4701b923122");
        in.putExtra("access_token","e28084439fe2407ebadb1bc44270473af0cd0417");
        in.putExtra("mobile", "+91"+mobileno);

        //Save username and mobile number in shared preferences for future use
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(getString(R.string.freecall_pref_username), "TODO");
        editor.putString(getString(R.string.freecall_pref_mobileno), mobileno);
        editor.commit();

        startActivityForResult(in,VerifyMobile.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int reqCode, int arg1, Intent arg2) {
        super.onActivityResult(reqCode, arg1, arg2);

        if (reqCode == VerifyMobile.REQUEST_CODE) {
            String message = arg2.getStringExtra("message");
            int result=arg2.getIntExtra("result", 0);

            //Result codes
            //101 = "MISSING CREDENTIALS";
            //102 = "MISSING REQUIRED VALUES";
            //103 = "MISSING PROPER NUMBER";
            //104 = "VERIFICATION SUCCESS";
            //105 = "NUMBER IS NOT CORRECT";
            //106 = "MOBILE NUMBER VERIFICATION CANCELED";
            //107 = "NETWORK ERROR CANNOT BE VERIFIED";
            //108 = "MOBILE NUMBER VERIFICATION FAILED, NO INTERNET";

            //test toast
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

            if(result == 104) {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(getString(R.string.freecall_pref_otp_auth_completed), true);
                editor.commit();

                String userName = pref.getString(getString(R.string.freecall_pref_username), "");
                String mobileNumber = pref.getString(getString(R.string.freecall_pref_mobileno), "");

                //TODO: show wait dialog with status updating server until Handler is called with status code
                Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        switch(msg.what) {
                            case ServerInterface.SERVER_IF_ADDUSER_INVALID_INPUT:
                                break;
                            case ServerInterface.SERVER_IF_ADDUSER_NWERR:
                                break;
                            case ServerInterface.SERVER_IF_ADDUSER_SERVER_RETURNED_ERR:
                                break;
                            case ServerInterface.SERVER_IF_ADDUSER_SUCCESS:
                                UserRegistration.this.finish();
                                break;
                        }
                    }
                };

                ServerInterface.newUserNotifyServer(handler, userName, mobileNumber);
            }
        }


    }
}
