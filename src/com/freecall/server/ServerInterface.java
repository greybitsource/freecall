package com.freecall.server;

import android.os.Handler;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by avinash.malipatil on 28-Jun-16.
 */
public class ServerInterface {

    public static final int SERVER_IF_ADDUSER_INVALID_INPUT = 100;
    public static final int SERVER_IF_ADDUSER_NWERR = 101;
    public static final int SERVER_IF_ADDUSER_SERVER_RETURNED_ERR = 102;
    public static final int SERVER_IF_ADDUSER_SUCCESS = 103;

    private static final String ADD_USER_URL = "http://?";

    private static class AddUserRequest implements Runnable {
        Handler mHandler;
        String mUserName;
        String mMobileNumber;

        AddUserRequest(Handler handler, String userName, String mobileNumber) {
            mHandler = handler;
            mUserName = userName;
            mMobileNumber = mobileNumber;
        }

        @Override
        public void run() {
            String link = ADD_USER_URL + mUserName + mMobileNumber;
            try {
                URL url = new URL(link);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                int resp = connection.getResponseCode();
                if(resp == 200) {
                    mHandler.sendEmptyMessage(SERVER_IF_ADDUSER_SUCCESS);
                }
                else {
                    mHandler.sendEmptyMessage(SERVER_IF_ADDUSER_SERVER_RETURNED_ERR);
                }
            } catch (MalformedURLException e) {
                mHandler.sendEmptyMessage(SERVER_IF_ADDUSER_INVALID_INPUT);
            } catch (IOException e) {
                mHandler.sendEmptyMessage(SERVER_IF_ADDUSER_NWERR);
            }
        }
    }

    public static void newUserNotifyServer(Handler handler, String userName, String mobileNumber) {
        new Thread(new AddUserRequest(handler, userName, mobileNumber)).start();
    }
}
